@extends('shop.layouts.index')

@section('content')

    <hr>
    <div class="row">
        <div class="col-md-4">
            <img src="{{\Illuminate\Support\Facades\Storage::url($product->image)}}" class="big-img">
        </div>
        <div class="col-md-8 d-flex flex-column">
                <p><b>Название: </b> {{$product->title}}</p>
                <p><b>Жанр: </b> {{($product->categories != null) ? $product->categories->name : "нету категории"}}</p>
                <p><b>Автор: </b> {{$product->author}}</p>
                <p><b>Описание: </b> {{$product->description}}</p>
                <p><b>Издатель: </b> {{$product->publishers->name}}</p>
                <p><b>Цена: </b> {{$product->price}} грн</p>
                <p><b>Оценка: </b> {{\App\Models\Rating::getRating($product->id)}}</p>
                <p><b>Есть ли на складе: </b> @available($product->available)</p>
        </div>
    </div>
    <hr>
    <div class="container mb-3">
        <div class="row justify-content-between">
            <div class="row">
                <p class="m-0 mr-3">Оценка книги: </p>
                <div>
                    <form action="{{route('shop.product.rating.add', $product)}}" method="POST">
                        @csrf
                        <button type="submit" name="rating" value="1" class="btn btn-danger">1</button>
                        <button type="submit" name="rating" value="2" class="btn btn-secondary">2</button>
                        <button type="submit" name="rating" value="3" class="btn btn-warning">3</button>
                        <button type="submit" name="rating" value="4" class="btn btn-primary">4</button>
                        <button type="submit" name="rating" value="5" class="btn btn-success">5</button>
                    </form>
                </div>
            </div>
            <form action="{{route('basket.add', $product)}}" method="POST">
                @csrf
                <button type="submit" class="btn btn-success">Купить</button>
            </form>
        </div>
    </div>


@endsection
