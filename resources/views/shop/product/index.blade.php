@extends('shop.layouts.index')

@section('content')

    @include('shop.layouts.filter')
    {{$products->links()}}

    <div class="row justify-content-around">
        @foreach($products as $product)
            @include('shop.layouts.card', $product)
        @endforeach
    </div>

    {{$products->links()}}

@endsection
