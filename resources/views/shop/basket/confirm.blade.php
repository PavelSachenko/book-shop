@extends('shop.layouts.index')

@section('content')
    <div class="row">
        <div class="card mb-3" style="width: 1000px;">
            <div class="card-header text-white-50 bg-success">Оформление заказа</div>
            <div class="card-body">
                <form action="{{route('basket.processing')}}" method="POST">
                    @csrf
                    @method('PUT')
                    <div class="form-group">
                        <label for="name">Имя:</label>
                        <input name="name" type="text" class="form-control" id="name" aria-describedby="emailHelp"
                               placeholder="Ваше имя" value="{{old('name')}}">
                        @include('shop.layouts.valid_error', ['param' => 'name'])
                    </div>
                    <div class="form-group">
                        <label for="phone">Телефон:</label>
                        <input name="phone" type="text" class="form-control" id="phone" aria-describedby="emailHelp"
                               placeholder="Телефон для связи" value="{{old('phone')}}">
                        @include('shop.layouts.valid_error', ['param' => 'phone'])
                    </div>
                    <div class="form-group">
                        <label for="email">Email:</label>
                        <input name="email" type="text" class="form-control" id="phone" aria-describedby="emailHelp"
                               placeholder="Email"  @auth value="{{Auth::user()->email}}" @endauth>
                        @include('shop.layouts.valid_error', ['param' => 'email'])

                    </div>
                    <div>
                        <p>Сума заказа: <b>{{$order->getFullSum()}} грн</b></p>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-success">Подтвердить</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection
