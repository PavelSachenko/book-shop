@extends('shop.layouts.index')

@section('content')
    <div class="row justify-content-center">

        @basket
        <div class="card border-success mb-3" style="width: 1000px;">
            <div class="card-header bg-success text-white">
                <div class="container">
                    <div class="row justify-content-between">
                        <p>Ваш заказ:</p>
                        <p class="mr-2">Итоговая цена: {{$order->getFullSum()}} грн</p>
                    </div>
                </div>
            </div>
            <div class="card-body">

                @foreach($order->products()->get() as $product)
                <div class="card text-white bg-info mb-3">
                    <div class="card-body">
                        <div class="row ml-3 justify-content-between d-flex align-items-center">
                            <div class="row col-md-6 m-0">
                                <p class="card-text  m-0"><b>Книга: </b> {{$product->title}}</p>
                                <p class="card-text  m-0 ml-2"><b>Автор: </b> {{$product->author}}</p>
                            </div>
                            <div class="row d-flex align-items-center col-md-6 justify-content-around">
                                <p class="card-text m-0">Количество: <b>{{$product->pivot->count}}</b> шт</p>
                                <form action="{{route('basket.add', $product)}}" method="POST">
                                    @csrf
                                    <button type="submit" class="btn btn-warning ">+</button>
                                </form>
                                <form action="{{route('basket.delete', $product)}}" method="POST">
                                    @csrf
                                    @method('delete')
                                    <button type="submit" class="btn btn-danger ">-</button>
                                </form>
                                <p class="card-text">Стоимость: <b>{{$product->price * $product->pivot->count}}</b> грн</p>
                            </div>
                        </div>
                    </div>

                </div>

                @endforeach
                <div class="container">
                    <div class="row justify-content-end">
                        <a href="{{route('basket.confirm')}}" type="button" class="btn btn-success">Купить</a>
                    </div>
                </div>
            </div>

        </div>
        @else
            <div class="card text-white bg-secondary mb-3" style="width: 1000px;">
                <div class="card-body">
                    <h4 class="card-title text-center">Ваша корзина пуста</h4>
                </div>
            </div>
        @endbasket
    </div>
@endsection
