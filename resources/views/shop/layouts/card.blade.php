<div class="card border-primary mb-5" style="max-width: 20rem;">
    <div class="card-header"><b>Автор:</b> {{$product->author}}</div>
    <div class="d-flex justify-content-center">
        @if($product->label_id != null)
            <div class="position-absolute mt-2 border text-center text-white" style="height: 25px; width: 200px; background: {{$product->labels->color}};">
                {{$product->labels->name}}
            </div>
        @endif
        <img src="{{\Illuminate\Support\Facades\Storage::url($product->image)}}" class="big-img mt-2">
    </div>
    <div class="card-body">
        <h4 class="card-title text-center">{{$product->title}}</h4>
        <p class="card-text">{{$product->description}}</p>
        <hr>
        <p class="card-text">Цена: <b>{{$product->price}} грн</b></p>
        <hr>
        <div class="row justify-content-around">
            <a href="{{route('shop.product.show', $product)}}" class="btn btn-info">Подробней</a>
            <form action="{{route('basket.add', $product)}}" method="POST">
                @csrf
                <button type="submit" class="btn btn-success @disabledbutton($product->available)"
                        @disabledbutton($product->available)>Купить</button>
            </form>
        </div>
    </div>
</div>
