<nav class="navbar navbar-expand-lg navbar-dark bg-primary">
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <div class="container">

        <ul class="navbar-nav mr-auto">

            <li class="nav-item @activelink('shop.index')">
                <a class="nav-link" href="{{route('shop.index')}}">Все книги</a>
            </li>
            <li class="nav-item @activelink('shop.categories')">
                <a class="nav-link" href="{{route('shop.categories')}}">Жанры</a>
            </li>
            <li class="nav-item @activelink('basket.index')">
                <a class="nav-link" href="{{route('basket.index')}}">Корзина</a>
            </li>
        </ul>
            <ul class="navbar-nav ml-auto">
                <!-- Authentication Links -->
                @guest
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('login') }}">Войти</a>
                    </li>
                    @if (Route::has('register'))
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('register') }}">Зарегестрироваться</a>
                        </li>
                    @endif
                @else
                    <li class="nav-item dropdown">
                        <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                            {{ Auth::user()->name }} <span class="caret"></span>
                        </a>

                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="{{route('shop.user.orders')}}">Заказы</a>
                            <a class="dropdown-item" href="{{ route('logout') }}"
                               onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                Выход
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                        </div>
                    </li>
                    @admin
                    <li class="nav-item">
                        <a href="{{route('admin.index')}}" class="nav-link">Админ Панель</a>
                    </li>
                    @endadmin
                @endguest
            </ul>
        </div>
    </div>
</nav>
