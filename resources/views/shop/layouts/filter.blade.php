<div class="container">

    <form action="{{route('shop.filter')}}" method="get">
        @csrf
        <div class="row justify-content-between">
            <div class="form-group">
                <input name="title" type="text" class="form-control" placeholder="Название книги">
            </div>
            <div class="custom-control custom-checkbox">
                <input name="available" type="checkbox" class="custom-control-input" id="customCheck1" checked>
                <label class="custom-control-label" for="customCheck1">Есть в наличии</label>
            </div>
            <div class="form-group">
                <input name="min_price" type="number" class="form-control"  placeholder="Цена от">
            </div>
            <div class="form-group">
                <input name="max_price" type="number" class="form-control"  placeholder="Цена до">
            </div>
            <div class="form-group">
                <button class="btn btn-secondary my-2 my-sm-0" type="submit">Search</button>
            </div>
        </div>
    </form>
</div>
