@extends('shop.layouts.index')

@section('css')
    <link rel="stylesheet" href="{{asset('css/custom_img.css')}}">
@endsection

@section('content')
    <div class="row d-flex justify-content-center">
        @foreach($categories as $category)
        <div class="card mb-3 col-md-3 mr-5">
                <h4 class="card-header text-center">{{$category->name}}</h4>
                <a href="{{route('shop.category', $category->name)}}" class="my-2"><img class="big-img" src="{{\Illuminate\Support\Facades\Storage::url($category->image)}}" alt="Card image"></a>
        </div>
        @endforeach
    </div>

@endsection
