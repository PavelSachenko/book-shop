@extends('shop.layouts.index')

@section('content')

    <div class="row">
        <table class="table table-hover">
            <thead>
            <tr  class="table-secondary">
                <th scope="col">Телефон</th>
                <th scope="col">Статус</th>
                <th scope="col">Был создан</th>
                <th scope="col">Общая стоимость</th>
            </tr>
            </thead>
            <tbody>
            @foreach($orders as $order)
                <tr class="table-info">
                    <td>{{$order->phone}}</td>
                    <td>{{$order->status}}</td>
                    <td>{{$order->created_at}}</td>
                    <td>{{$order->getFullSum()}}</td>
                </tr>
            @endforeach

            </tbody>
        </table>
    </div>
@endsection
