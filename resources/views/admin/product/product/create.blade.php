@extends('admin.layouts.index')
@section('css')
    <link rel="stylesheet" href="{{asset('css/custom_img.css')}}">
@endsection
@section('content')
    <div class="row">
        <div class="card border-primary mb-3 сщд-12" style="width: 1025px;">
            <div class="card-header bg-primary text-success">Продукт: </div>
            <div class="card-body text-primary">
                <form action="{{ isset($product) ? route('admin.product.update', $product) : route('admin.product.store')}}" method="POST" enctype="multipart/form-data">
                    @csrf
                    @if(isset($product))
                        @method('PUT')
                    @endif
                    <div class="row form-group">
                        <div class="col-6">
                            <label>Название книги:</label>
                            <input name="title" type="text" class="form-control" placeholder="Имя книги" value="{{isset($product) ? $product->title : old('title')}}">
                            @include('admin.layouts.valid_error', ['param' => 'title'])
                        </div>
                        <div class="col-6">
                            <label>Автор:</label>
                            <input name="author" type="text" class="form-control" placeholder="Автор" value="{{isset($product) ? $product->author : old('author')}}">
                            @include('admin.layouts.valid_error', ['param' => 'author'])
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-12">
                            <label>Описание:</label>
                            <textarea  name="description" class="form-control"  placeholder="Описание" rows="8">{{isset($product) ? $product->description : old('description')}}</textarea>
                            @include('admin.layouts.valid_error', ['param' => 'description'])
                        </div>
                    </div>

                    <div class="row form-group">
                        <div class="col-6">
                            <label>Категория:</label>
                            <select name="category_id"  onselect="{{isset($product) ? $product->category_id : ''}}{{old('category')}}" class="form-control" id="exampleFormControlSelect1">
                                @foreach(\App\Models\Rest\Category::all() as $category)
                                    <option value="{{$category->id}}" @if(isset($product) && $category->id == $product->category_id) selected @endif>{{$category->name}}</option>
                                @endforeach
                            </select>
                            @include('admin.layouts.valid_error', ['param' => 'category'])

                        </div>
                        <div class="col-6">
                            <label>Издатель:</label>
                            <select name="publisher_id" class="form-control" id="exampleFormControlSelect1">
                                @foreach(\App\Models\Rest\Publisher::all() as $publisher)
                                    <option value="{{$publisher->id}}" @if(isset($product) && $publisher->id == $product->publisher_id) selected @endif>{{$publisher->name}}</option>
                                @endforeach
                            </select>
                            @include('admin.layouts.valid_error', ['param' => 'publisher'])
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-6">
                            <label>Страна:</label>
                            <select name="country_id" onselect="{{isset($product) ? $product->country_id : ''}}" class="form-control" id="exampleFormControlSelect1">
                                @foreach(\App\Models\Rest\Country::all() as $country)
                                    <option value="{{$country->id}}" @if(isset($product) && $country->id == $product->country_id) selected @endif>{{$country->country}}</option>
                                @endforeach
                            </select>
                            @include('admin.layouts.valid_error', ['param' => 'country'])
                        </div>
                        <div class="col-6">
                            <label>Стоимость:</label>
                            <input name="price" type="number" class="form-control" placeholder="Цена" value="{{isset($product) ? $product->price : old('price')}}">
                            @include('admin.layouts.valid_error', ['param' => 'price'])
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-6">
                            <label>Количество на складе:</label>
                            <input name="count" type="number" class="form-control" placeholder="Количество на складе, по умолчанию 0" value="{{isset($product) ? $product->count : old('count')}}">
                            @include('admin.layouts.valid_error', ['param' => 'count'])
                        </div>
                        <div class="col-6">
                            <label>Лейбл:</label>
                            <select name="label_id" onselect="{{isset($product) ? $product->label_id : ''}}" class="form-control" id="label">
                                    <option value="" selected>Без лейбла</option>
                                @foreach(\App\Models\Rest\Label::all() as $label)
                                    <option value="{{$label->id}}" @if(isset($product) && $label->id == $product->label_id) selected @endif>{{$label->name}}</option>
                                @endforeach
                            </select>
                            @include('admin.layouts.valid_error', ['param' => 'label'])
                        </div>

                    </div>
                    <div class="row">
                        <div class="col-6">
                            <div class="custom-file mt-2">
                                <input name="image" type="file" class="custom-file-input" id="customFile">
                                <label class="custom-file-label" for="customFile">Выберите Изображения</label>
                                @include('admin.layouts.valid_error', ['param' => 'image'])

                            </div>
                        </div>
                        <div class="col-5 ml-4 mt-4">
                            <input name="available" type="hidden" value="off">
                            <input name="available" {{isset($product) ? ($product->available) ? "checked" : '' : ''}} class="form-check-input" type="checkbox" id="inlineCheckbox1" value="on">
                            <label class="form-check-label" for="inlineCheckbox1">
                                Доступен
                            </label>
                        </div>
                    </div>
                    @if(isset($product))
                        <div class="row">
                            <div class="col-6">
                                <img src="{{\Illuminate\Support\Facades\Storage::url($product->image)}}" class="medium-img ml-5"/>
                            </div>
                        </div>
                    @endif
                    <div class="row mt-3">
                        <div class="col-3">
                            <a href="{{route('admin.product.index')}}" class="form-control btn btn-dark">Все товары</a>
                        </div>
                        <div class="col-3">
                            <button type="submit" class="form-control btn btn-success">@if(isset($product)) Изменить @else Добавить @endif</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>

    </div>
@endsection
