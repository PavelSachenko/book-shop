@extends('admin.layouts.index')
@section('css')
    <link rel="stylesheet" href="{{asset('css/custom_img.css')}}">
@endsection
@section('content')

    <div class="row">
        <div class="card border-primary mb-3 сщд-12" style="width: 1025px;">
            <div class="card-header bg-primary text-success">Все о продукте: </div>
            <div class="card-body text-primary justify-content-center">
                <div class="card text-white bg-primary mb-3" style="width: 100%;">
                    <div class="card-header">Главная</div>
                    <div class="card-body">
                            <h6 class="card-title">Название книги:</h6>
                            <span class="card-text text-muted">{{$product->title}}</span>
                            <h6 class="card-title mt-2">Описание:</h6>
                            <span class="card-text text-muted">{{$product->description}}</span>
                            <h6 class="card-title mt-2">Автор:</h6>
                            <span class="card-text text-muted">{{$product->author}}</span>
                            <h6 class="card-title mt-2">Жанр:</h6>
                            <span class="card-text text-muted">{{($product->categories != null) ? $product->categories->name : "null"}}</span>
                            <h6 class="card-title mt-2">Цена:</h6>
                            <span class="card-text text-muted">{{$product->price}} грн</span>
                            <h6 class="card-title mt-2">Доступна:</h6>
                            <span class="card-text text-muted">{{($product->available) ? "Да" : "Нет" }}</span>
                            <h6 class="card-title mt-2">Лейблы: </h6>
                            <span class="card-text text-muted">раз два три</span>
                            <h6 class="card-title mt-2">Картинка: </h6>
                            <img src="{{\Illuminate\Support\Facades\Storage::url($product->image)}}" class="big-img">
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
@endsection
