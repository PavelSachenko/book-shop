@extends('admin.layouts.index')
@section('css')
    <link rel="stylesheet" href="{{asset('css/custom_img.css')}}">
@endsection
@section('content')
    <div class="row justify-content-end">
        <a href="{{route('admin.product.create')}}" class="btn btn-primary mb-2">Добавить товар</a>
    </div>
    <div class="row">
        <table class="table table-hover">
            <thead class="table-primary">
            <tr>
                <th scope="col">Название</th>
                <th scope="col">Автор</th>
                <th scope="col">На складе</th>
                <th scope="col">Цена</th>
                <th scope="col">Категория</th>
                <th scope="col">Издатель</th>
                <th scope="col">Страна</th>
                <th scope="col">Картинка</th>
                <th scope="col">Доступен</th>
                <th scope="col">Действия</th>
            </tr>
            </thead>
            <tbody>
            @foreach($products as $product)
                <tr class="table-dark">
                    <th>{{$product->title}}</th>
                    <td>{{$product->author}}</td>
                    <td>{{$product->count}}</td>
                    <td>{{$product->price}}</td>
                    <td>{{($product->categories != null) ? $product->categories->name : "null"}}</td>
                    <td>{{$product->publishers->name}}</td>
                    <td>{{$product->countries->country}}</td>
                    <td><img src="{{\Illuminate\Support\Facades\Storage::url($product->image)}}" class="small-img"></td>
                    <td>{{($product->available) ? "Да" : "Нет"}}</td>
                    <td>
                        <form action="{{route('admin.product.destroy', $product)}}" method="POST"class="d-inline">
                            @csrf
                            @method('DELETE')
                            <button type="submit" class="btn btn-danger">delete</button>
                        </form>
                        <a href="{{route('admin.product.edit', $product)}}" class="btn btn-warning">edit</a>
                        <a href="{{route('admin.product.show', $product)}}" class="btn btn-info">overview</a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        {{$products->links()}}
    </div>
@endsection
