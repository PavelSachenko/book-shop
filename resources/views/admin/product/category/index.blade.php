@extends('admin.layouts.index')
@section('css')
    <link rel="stylesheet" href="{{asset('css/custom_img.css')}}">
@endsection
@section('content')
    <div class="row justify-content-end">
        <a href="{{route('admin.category.create')}}" class="btn btn-primary mb-2">Добавить категорию</a>
    </div>
    <div class="row">
        <table class="table table-hover">
            <thead class="table-primary">
            <tr>
                <th scope="col">Id</th>
                <th scope="col">Категория</th>
                <th scope="col">картинка</th>
                <th scope="col">Действия</th>
            </tr>
            </thead>
            <tbody>
                @foreach($categories as $category)
                    <tr class="table-dark">
                        <th>{{$category->id}}</th>
                        <td>{{$category->name}}</td>
                        <td><img src="{{\Illuminate\Support\Facades\Storage::url($category->image)}}" class="small-img"></td>
                        <td>
                            <form action="{{route('admin.category.destroy', $category)}}" method="POST"class="d-inline">
                                @csrf
                                @method('DELETE')
                                <button type="submit" class="btn btn-danger">delete</button>
                            </form>
                            <a href="{{route('admin.category.edit', $category)}}" class="btn btn-warning">edit</a>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        {{$categories->links()}}
    </div>
@endsection
