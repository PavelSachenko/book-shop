@extends('admin.layouts.index')
@section('css')
    <link rel="stylesheet" href="{{asset('css/custom_img.css')}}">
@endsection

@section('content')

    <div class="row">

        <div class="card border-primary mb-3 сщд-12" style="width: 525px;">
            <div class="card-header bg-primary text-success">Новая категория</div>
            <div class="card-body text-primary">
                <form action="{{ isset($category) ? route('admin.category.update', $category) : route('admin.category.store')}}" method="POST" enctype="multipart/form-data">
                    @csrf
                    @if(isset($category))
                        @method('PUT')
                    @endif
                    <div class="row">
                        <div class="col-9">
                            <input name="name" type="text" class="form-control" placeholder="Категория" value="@if(isset($category)){{$category->name}}@endif{{old('name')}}">
                            @include('admin.layouts.valid_error', ['param' => 'name'])
                            <div class="custom-file mt-2">
                                <input name="image" type="file" class="custom-file-input" id="customFile">
                                <label class="custom-file-label" for="customFile">Выберите Изображения</label>
                            </div>
                            @include('admin.layouts.valid_error', ['param' => 'image'])
                        </div>
                        <div class="col-3">
                            <button type="submit" class="form-control btn btn-success">@if(isset($category)) Изменить @else Добавить @endif</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    @if(isset($category))
    <img src="{{\Illuminate\Support\Facades\Storage::url($category->image)}}" class="medium-img ml-5"/>
    @endif
</div>
@endsection
