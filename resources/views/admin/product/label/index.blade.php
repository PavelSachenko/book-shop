@extends('admin.layouts.index')

@section('content')
    <div class="row justify-content-end">
        <a href="{{route('admin.label.create')}}" class="btn btn-primary mb-2">Добавить лейбл</a>
    </div>
    <div class="row">
        <table class="table table-hover">
            <thead class="table-primary">
            <tr>
                <th scope="col">Id</th>
                <th scope="col">Название</th>
                <th scope="col">Цвет</th>
                <th scope="col">Действия</th>
            </tr>
            </thead>
            <tbody>
            @foreach($labels as $label)
                <tr class="table-dark">
                    <th>{{$label->id}}</th>
                    <td>{{$label->name}}</td>
                    <td><div style="height: 30px; width: 30px; background: {{$label->color}}"></div></td>
                    <td>
                        <form action="{{route('admin.label.destroy', $label)}}" method="POST"class="d-inline">
                            @csrf
                            @method('DELETE')
                            <button type="submit" class="btn btn-danger">delete</button>
                        </form>
                        <a href="{{route('admin.label.edit', $label)}}" class="btn btn-warning">edit</a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        {{$labels->links()}}
    </div>
@endsection
