@extends('admin.layouts.index')

@section('content')
    <div class="row">

        <div class="card border-primary mb-3 сщд-12" style="width: 525px;">
            <div class="card-header bg-primary text-success">Новый лейбл</div>
            <div class="card-body text-primary">
                <form action="{{ isset($label) ? route('admin.label.update', $label) : route('admin.label.store')}}" method="POST">
                    @csrf
                    @if(isset($label))
                        @method('PUT')
                    @endif
                    <div class="row">
                        <div class="col-9">
                            <input name="name" type="text" class="form-control" placeholder="Лейбл" value="@if(isset($label)){{$label->name}}@endif{{old('name')}}">
                            @include('admin.layouts.valid_error', ['param' => 'name'])
                        </div>
                        <div class="col-9 mt-3">
                            <input name="color" type="color" class="form-control" placeholder="Цвет" value="@if(isset($label)){{$label->color}}@endif{{old('color')}}">
                            @include('admin.layouts.valid_error', ['param' => 'name'])
                        </div>
                        <div class="col-3 mt-3">
                            <button type="submit" class="form-control btn btn-success">@if(isset($label)) Изменить @else Добавить @endif</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>

    </div>
@endsection
