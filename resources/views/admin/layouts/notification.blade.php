@if(session()->has('success'))
    <div class="row">
        <div class="alert alert-dismissible alert-success" style="width: 525px">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <p>{{session()->get('success')}}</p>
        </div>
    </div>
@elseif(session()->has('warning'))
    <div class="row">
        <div class="alert alert-dismissible alert-warning" style="width: 525px">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <p>{{session()->get('warning')}}</p>
        </div>
    </div>
@elseif(session()->has('danger'))
    <div class="row">
        <div class="alert alert-dismissible alert-danger" style="width: 525px">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <p>{{session()->get('danger')}}</p>
        </div>
    </div>
@endif
