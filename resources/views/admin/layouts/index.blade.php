<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" href="{{asset('css/navbar/bootstrap.css')}}">
    <link rel="stylesheet" href="{{asset('css/navbar/navbar-fixed-left.css')}}">
    @yield('css')
    @yield('headerscript')
    <title>@yield('title')</title>
</head>
<body class="bg-secondary">
@include('admin.navbar')


<div class="container mt-5">
    @include('admin.layouts.notification')
    @yield('content')
</div>


@yield('bodyscript')
<script src="{{asset('js/app.js')}}"></script>
</body>
</html>
