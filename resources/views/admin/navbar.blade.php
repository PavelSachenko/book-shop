<nav class="navbar navbar-expand-lg navbar-dark bg-primary fixed-left">
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item ">
                <a class="nav-link" href="{{route('shop.index')}}">Магазин </a>
            </li>
            <li class="nav-item @activelink('admin.statistic')">
                <a class="nav-link" href="{{route('admin.statistic')}}">Статистика </a>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Товар
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item @activelink('admin.category.index')" href="{{route('admin.category.index')}}">Категория</a>
                    <a class="dropdown-item @activelink('admin.product.index')" href="{{route('admin.product.index')}}">Товар</a>
                    <a class="dropdown-item @activelink('admin.label.index')"href="{{route('admin.label.index')}}">Лейбл</a>
                </div>
            </li>
            <li class="nav-item dropdown  @activelink('admin.orders')">
                    <a class="nav-link" href="{{route('admin.orders')}}">Заказы</a>
            </li>
            <li class="nav-item  @activelink('admin.users')">
                <a class="nav-link" href="{{route('admin.users')}}">Пользователи</a>
            </li>

        </ul>
    </div>
</nav>
