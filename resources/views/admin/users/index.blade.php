@extends('admin.layouts.index')

@section('content')
    <div class="row">
        <table class="table table-hover">
            <thead class="table-primary">
            <tr>
                <th scope="col">Имя</th>
                <th scope="col">Телефон</th>
                <th scope="col">Деньги</th>
                <th scope="col">Роль</th>
                <th scope="col">К-во заказов</th>
                <th scope="col">Действие</th>
            </tr>
            </thead>
            <tbody>
            @foreach($users as $user)
                <tr class="table-dark">
                    <th class="m-0">{{$user->name}}</th>
                    <th class="m-0">{{$user->email}}</th>
                    <th class="m-0">{{$user->money}}</th>
                    <th class="m-0">{{$user->role->name}}</th>
                    <th class="m-0">{{count($user->orders)}}</th>
                    <th class="m-0">
                        @if($user->role_id != 3)
                        <form action="{{route('admin.user.update', $user)}}" method="POST">
                            @csrf
                            @method('PUT')
                            <button type="submit" class="btn btn-info">Дать админку</button>
                        </form>
                        @endif
                        <form action="{{route('admin.user.destroy', $user)}}" method="POST">
                            @csrf
                            @method('DELETE')
                            <button type="submit" class="btn btn-danger">Удалить</button>
                        </form>
                    </th>
                </tr>
            @endforeach
            </tbody>
        </table>
        {{$users->links()}}
    </div>
@endsection
