@extends('admin.layouts.index')
@section('headerscript')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.3/Chart.min.js"></script>
@endsection
@section('content')

    <div class="row justify-content-between">
        <div class="card text-white bg-primary mb-3" style="width: 20rem;">
            <div class="card-header">Всего пользователей</div>
            <div class="card-body">
                <h4 class="card-title"> {{count($data['users'])}}</h4>
            </div>
        </div>
        <div class="card text-white bg-primary mb-3" style="width: 20rem;">
            <div class="card-header">Всего заказов</div>
            <div class="card-body">
                <h4 class="card-title"> {{count($data['orders'])}}</h4>
            </div>
        </div>
        <div class="card text-white bg-primary mb-3" style="width: 20rem;">
            <div class="card-header">Всего продуктов</div>
            <div class="card-body">
                <h4 class="card-title"> {{count($data['products'])}}</h4>
            </div>
        </div>
    </div>

    <canvas id="line-chart" width="600" height="250"></canvas>
    <div class="row">
        <canvas id="pie-chart-product" class="col-md-6"></canvas>
        <canvas id="pie-chart-order" class="col-md-6"></canvas>
    </div>
    <script>
        const data = {!! json_encode($data) !!};
        // console.log(data['orders'][0]['status']);
        new Chart(document.getElementById("line-chart"), {
            type: 'line',
            data: {
                labels: data['line_chart']['dates'],
                datasets: [{
                    data: data['line_chart']['users'],
                    label: "Пользователи",
                    borderColor: "#3e95cd",
                    fill: false
                }, {
                    data: data['line_chart']['orders'],
                    label: "Заказы",
                    borderColor: "#8e5ea2",
                    fill: false
                }]
            },
        });


        new Chart(document.getElementById("pie-chart-product"), {
            type: 'pie',
            data: {
                labels: ["Активных", "Неактивных"],
                datasets: [{
                    backgroundColor: ["#3BCD9B", "#2c3e50"],
                    data: [available = function () {
                        let sum = 0;
                        for(let i in data['products']){
                            if(data['products'][i]['available'] == 1)
                                sum ++;
                        }
                        return sum;
                    }(), data['products'].length - available]
                }]
            },
            options: {
                title: {
                    display: true,
                    text: 'Продукты'
                }
            }
        });
        new Chart(document.getElementById("pie-chart-order"), {
            type: 'pie',
            data: {
                labels: ["Выполненные", "Не выполненные"],
                datasets: [{
                    backgroundColor: ["#3bcd9b", "#2C3E50"],
                    data: [performed = function () {
                        let sum = 0;
                        for(let i in data['orders']){
                            if(data['orders'][i]['status'] == "Выполнено")
                                sum ++;
                        }
                        return sum;
                    }(), data['orders'].length - performed]
                }]
            },
            options: {
                title: {
                    display: true,
                    text: 'Заказы'
                }
            }
        });
    </script>

@endsection

