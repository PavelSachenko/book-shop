@extends('admin.layouts.index')

@section('content')
    <div class="card border-primary mb-3" style="width: 1000px;">
        <div class="card-header bg-primary text-white">
            <div class="container">
                <div class="row justify-content-between">
                    <p class="mr-2 m-0">Итоговая цена: {{$order->getFullSum()}} грн</p>
                </div>
            </div>
        </div>
        <div class="card-body">

            @foreach($order->products()->get() as $product)
                <div class="card text-white bg-dark mb-3">
                    <div class="card-body">
                        <div class="row ml-3 justify-content-between d-flex align-items-center">
                            <form action="{{route('admin.order.update', $order)}}" method="POST" class="col-md-8">
                                @csrf
                                @method('PUT')
                                    <div class="from-group">
                                        <label for="status">Изменить статус</label>
                                        <select name="status" class="form-control" id="status">
                                            <option value="1">В обработке</option>
                                            <option value="2">Выполнено</option>
                                            <option value="3">Отмененно</option>
                                        </select>
                                    </div>
                                    <button type="submit" class="btn btn-success mt-2">Изменить</button>
                            </form>
                        </div>
                    </div>

                </div>

            @endforeach
        </div>
    </div>
@endsection
