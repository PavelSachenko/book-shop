@extends('admin.layouts.index')

@section('content')
    <div class="row">
        <table class="table table-hover">
            <thead class="table-primary">
            <tr>
                <th scope="col">Телефон</th>
                <th scope="col">Имя</th>
                <th scope="col">Сума</th>
                <th scope="col">Статус</th>
                <th scope="col">Действия</th>
            </tr>
            </thead>
            <tbody>
            @foreach($orders as $order)
                <tr class="table-dark">
                    <th class="m-0">{{$order->phone}}</th>
                    <th class="m-0">{{$order->name}}</th>
                    <th class="m-0">{{$order->getFullSum()}}</th>
                    <th class="m-0">{{$order->status}}</th>
                    <td>
                        <form action="" method="POST"class="d-inline">
                            @csrf
                            @method('DELETE')
                            <button type="submit" class="btn btn-danger">delete</button>
                        </form>
                    <a href="{{route('admin.order.edit', $order)}}" class="btn btn-warning">edit</a>
                    <a href="{{route('admin.order', $order)}}" class="btn btn-info">overview</a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        {{$orders->links()}}
    </div>
@endsection
