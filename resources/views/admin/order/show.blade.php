@extends('admin.layouts.index')

@section('content')
    <div class="card border-primary mb-3" style="width: 1000px;">
        <div class="card-header bg-primary text-white">
            <div class="container">
                <div class="row justify-content-between">
                    <p class="mr-2 m-0">Итоговая цена: {{$order->getFullSum()}} грн</p>
                </div>
            </div>
        </div>
        <div class="card-body">

            @foreach($order->products()->get() as $product)
                <div class="card text-white bg-dark mb-3">
                    <div class="card-body">
                        <div class="row ml-3 justify-content-between d-flex align-items-center">
                            <div class="row col-md-6 m-0">
                                <p class="card-text  m-0"><b>Книга: </b> {{$product->title}}</p>
                                <p class="card-text  m-0 ml-2"><b>Автор: </b> {{$product->author}}</p>
                            </div>
                            <div class="row d-flex align-items-center col-md-6 justify-content-around">
                                <p class="card-text m-0">Количество: <b>{{$product->pivot->count}}</b> шт</p>
                                <p class="card-text">Стоимость: <b>{{$product->price * $product->pivot->count}}</b> грн</p>
                            </div>
                        </div>
                    </div>

                </div>

            @endforeach
        </div>
    </div>
@endsection
