<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('/clear', 'Shop\BasketController@clear');

Route::get('/', 'Shop\MainController@index')->name('shop.index');
Route::get('/product/{product}/show', 'Shop\MainController@show')->name('shop.product.show');

Route::get('/categories', 'Shop\MainController@categories')->name('shop.categories');
Route::get('/category/{category}', 'Shop\MainController@category')->name('shop.category');

Route::get('/filtered', 'Shop\MainController@filter')->name('shop.filter');

Route::group(['middleware' => 'auth'], function (){
    Route::get('/orders', 'Shop\OrderController@index')->name('shop.user.orders');
    Route::post('/product/{product}/rating', 'Shop\RatingController@add')->name('shop.product.rating.add');
});

Route::group(['prefix' => 'basket'], function (){

    Route::get('/', 'Shop\BasketController@index')->name('basket.index');
    Route::post('/{product}/add', 'Shop\BasketController@add')->name('basket.add');
    Route::delete('/{product}/delete', 'Shop\BasketController@delete')->name('basket.delete');

   Route::group(['middleware' => 'order'], function (){
        Route::get('/confirm', 'Shop\BasketController@confirm')->name('basket.confirm');
        Route::put('/processing', 'Shop\BasketController@processing')->name('basket.processing');
   });
});


Route::group(['prefix' => 'admin', 'middleware' => ['auth', 'admin']],function (){
    Route::get('/', 'Admin\HomeController@index')->name('admin.index');

    Route::get('/orders', 'Admin\OrderController@index')->name('admin.orders');

    Route::group(['prefix' => 'order'], function (){
        Route::get('/{order}', 'Admin\OrderController@show')->name('admin.order');
        Route::get('/{order}/edit', 'Admin\OrderController@edit')->name('admin.order.edit');
        Route::put('/{order}/update', 'Admin\OrderController@update')->name('admin.order.update');
    });

    Route::group(['prefix' => 'users'], function (){
        Route::get('/', 'Admin\UserController@index')->name('admin.users');
        Route::put('/{user}/update', 'Admin\UserController@update')->name('admin.user.update');
        Route::delete('/{user}/destroy', 'Admin\UserController@destroy')->name('admin.user.destroy');
    });

    Route::get('/statistics', 'Admin\StatisticController@index')->name('admin.statistic');

    Route::group(['prefix' => 'product'], function (){
        Route::resource('/label', 'Admin\Product\LabelController')->names('admin.label');
        Route::resource('/category', 'Admin\Product\CategoryController')->names('admin.category');
        Route::resource('/product', 'Admin\Product\ProductController')->names('admin.product');
    });
});


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
