<?php

use Illuminate\Database\Seeder;

class BookCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \Illuminate\Support\Facades\DB::table('categories')->insert([
            ['name' => 'fantasy', 'image' => 'image/no-img.png'],
            ['name' => 'roman', 'image' => 'image/no-img.png'],
            ['name' => 'science', 'image' => 'image/no-img.png'],
            ['name' => 'history', 'image' => 'image/no-img.png'],
            ['name' => 'adventure', 'image' => 'image/no-img.png'],
        ]);
    }
}
