<?php

use Illuminate\Database\Seeder;

class BookCountrySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \Illuminate\Support\Facades\DB::table('countries')->insert([
            ['country' => 'Ukraine'],
            ['country' => 'Poland'],
            ['country' => 'Germany'],
            ['country' => 'USA'],
        ]);
    }
}
