<?php

use Illuminate\Database\Seeder;

class LabelSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \Illuminate\Support\Facades\DB::table('labels')->insert([
           ['name' => 'Новинка'],
           ['name' => 'Хит'],
           ['name' => 'Популярное'],
        ]);
    }
}
