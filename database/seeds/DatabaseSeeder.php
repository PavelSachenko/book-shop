<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(BookCountrySeeder::class);
        $this->call(BookCategorySeeder::class);
        $this->call(BookPublisherSeeder::class);
        $this->call(LabelSeeder::class);
        $this->call(BookSeeder::class);
        $this->call(RoleSeeder::class);
        $this->call(UserSeeder::class);
    }
}
