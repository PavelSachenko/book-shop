<?php

use Illuminate\Database\Seeder;

class BookPublisherSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \Illuminate\Support\Facades\DB::table('publishers')->insert([
           ['name' => 'White Raven', 'country_id' => rand(1, 4)],
           ['name' => 'Red Rabbit', 'country_id' => rand(1, 4)],
           ['name' => 'Blue Rat', 'country_id' => rand(1, 4)],
           ['name' => 'Pink Elephant', 'country_id' => rand(1, 4)],
        ]);
    }
}
