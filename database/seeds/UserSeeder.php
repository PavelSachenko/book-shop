<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \Illuminate\Support\Facades\DB::table('users')->insert([
            'name' => 'Admin',
            'email' => 'a@gmail.com',
            'password' => bcrypt('12345678'),
            'remember_token' => Str::random(10),
            'email_verified_at' => now(),
            'role_id' => 3,
            'money' => 1000
        ]);

        factory(\App\User::class, 4)->create();
    }
}
