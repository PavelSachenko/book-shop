<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Http\SpecialClasses\Date;
use Faker\Generator as Faker;

$factory->define(\App\Models\Rest\Book::class, function (Faker $faker) {
    $date = Date::getRandomDate(config('statistic.statistic_date_start'), \Carbon\Carbon::now(), 'Y-m-d H:i:s');
    return [
        'title' => $faker->word,
        'published' => \Carbon\Carbon::now(),
        'price' => $faker->randomFloat(2, 1, 2000),
        'description' => $faker->text,
        'available' => $faker->boolean(),
        'author' => $faker->firstName . ' ' . $faker->lastName,
        'count' => rand(0, 30),
        'image' => 'image/no-img.png',

        'publisher_id' => rand(1, 4),
        'country_id' => rand(1, 4),
        'category_id' => rand(1, 5),

        'created_at' => $date,
        'updated_at' => $date,
    ];
});
