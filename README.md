# Installation

1. run composer install to generate depedencies in vendor folder
2. change .env.example to .env. Open .env and change configuration on yours 
#### Settings for DB
DB_CONNECTION=  
DB_HOST=  
DB_PORT=  
DB_DATABASE=  
DB_USERNAME=  
DB_PASSWORD=  
#### Settings for email
MAIL_MAILER=  
MAIL_HOST=  
MAIL_PORT=  
MAIL_USERNAME=  
MAIL_PASSWORD=  
MAIL_ENCRYPTION=  
MAIL_FROM_ADDRESS=shop@gmail.com  
MAIL_FROM_NAME="${APP_NAME}"  
#### Settings for the first date of countdown
STATISTIC_DATE_START=2020-06-29  

3. run the command in the console: **php artisan key:generate**
4.  run the command in the console: **php artisan migrate --seed**
5. run database server
6. run the command in the console: **php artisan serve** 
7. run the command in the console: **php artisan queue:work**
8. follow the link "/" on your site
9. Complete !!!

# Administrator access rights
> email: a@gmail.com
>password: 12345678
