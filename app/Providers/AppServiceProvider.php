<?php

namespace App\Providers;

use App\Models\Order;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Blade::directive('available', function ($product){
            return "<?php echo ($product) ? 'Да' : 'Нет';  ?>";
        });

        Blade::directive('activelink', function ($route){
            return "<?php echo (request()->route()->named($route)) ? 'active' : '' ?>";
        });

        Blade::if('basket', function (){
            if(!is_null(session('order_id')) && Order::find(session('order_id'))->getFullSum() != 0){
                return true;
            }
            return false;
        });

        Blade::if('admin', function (){
            return Auth::user()->isAdmin();
        });

        Blade::directive('disabledbutton', function ($product){
            return "<?php echo ($product) ? '' : 'disabled'; ?>";
        });
    }
}
