<?php

namespace App\Http\Resources;

use App\Models\Rest\Category;
use App\Models\Rest\Country;
use App\Models\Rest\Publisher;
use Illuminate\Http\Resources\Json\JsonResource;

class Book extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'title' => $this->title,
            'price' => $this->price,
            'description' => $this->description,
            'author' => $this->author,
            'available' => $this->available,
            'published' => $this->published,
            'count' => $this->count,

            'category' => Category::find($this->category_id)->name,
            'publisher' => Publisher::find($this->publisher_id)->name,
            'country' => Country::find($this->country_id)->country
        ];
    }
}
