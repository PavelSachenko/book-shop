<?php

namespace App\Http\Middleware\Shop;

use App\Models\Order;
use Closure;

class BasketNotEmpty
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!is_null(session('order_id')) && Order::find(session('order_id'))->getFullSum() != 0){
            return $next($request);
        }
        return redirect()->route('shop.index');
    }
}
