<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class ProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|min:3|max:20',
            'author' => 'required|min:3|max:20',
            'description' => 'required|min:5|max:500',
            'price' => 'required|numeric',
            'count' => 'numeric',
            'category_id' => 'required',
            'country_id' => 'required',
            'publisher_id' => 'required',
            'image' => 'mimes:jpeg,jpg,png,gif'
        ];
    }
}
