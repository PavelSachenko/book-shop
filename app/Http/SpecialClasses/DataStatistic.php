<?php


namespace App\Http\SpecialClasses;


use App\Models\Order;
use App\Models\Rest\Book;
use App\User;
use Carbon\Carbon;

class DataStatistic
{

    private $collection;
    private $data;

    private function formatDate($collection){
        return $collection->map(function ($item){
            return Carbon::parse($item->created_at)->format('m/d');
        });
    }


    private function fillOrderUserDate($orders, $users){
        $this->collection['dates'] = Date::generateDateRange(config('statistic.statistic_date_start'), Carbon::now()->format('y-m-d'));
        foreach ($this->collection['dates'] as $date){
            $order = 0;
            $user = 0;
            foreach ($this->formatDate($orders) as $item){
                if($date == $item){
                    $order++;
                }
            }
            foreach ($this->formatDate($users) as $item){
                if($date == $item){
                    $user++;
                }
            }
            $this->collection['orders']->push($order);
            $this->collection['users']->push($user);
        }
    }

    private function createData(){
        $orders = Order::all();
        $users = User::all();
        $product = Book::all();

        $this->data['orders'] = $orders;
        $this->data['users'] = $users;
        $this->data['products'] = $product;

        $this->collection = collect(['dates' => collect(), 'orders' => collect(), 'users' => collect()]);

        $this->fillOrderUserDate($orders, $users);
        $this->data['line_chart'] = $this->collection;

    }


    public function getData(){
        $this->createData();
//        dd($this->data);
        return $this->data;
    }
}
