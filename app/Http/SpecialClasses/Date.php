<?php


namespace App\Http\SpecialClasses;


use Carbon\CarbonPeriod;

class Date
{
    public static function generateDateRange($first_date, $last_date, $format = 'm/d'){
        $date = [];
        $period = CarbonPeriod::create($first_date, $last_date);
        foreach ($period as $item) {
            array_push($date,$item->format($format));
        }
        return $date;
    }

    public static function getRandomDate($first_date, $last_date, $format = 'm/d'){
        $date = self::generateDateRange($first_date, $last_date, $format);
        return $date[rand(0, count($date)-1)];
    }
}
