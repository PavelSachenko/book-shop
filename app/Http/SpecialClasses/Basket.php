<?php


namespace App\Http\SpecialClasses;


use App\Events\DeleteOrderEvent;
use App\Models\Order;
use Illuminate\Support\Facades\Auth;

class Basket
{
    private function orderFind($product_id){
        return Order::find(session('order_id'))->products()->where('book_id', $product_id)->first()->pivot;
    }

    private function productIncrement($order, $product_id){
        if($order->products->contains($product_id)){
            $order = $this->orderFind($product_id);
            $order->count++;
            $order->update();
        }else{
            $order->products()->attach($product_id);
        }
    }

    private function productDecrement($order){
        if($order->count > 1){
            $order->count--;
            $order->update();
        }else{
            $order->delete();
        }
    }

    public function add($product){
        if($this->checkAvailable($product)){
            $orderId = session('order_id');
            if(is_null($orderId)){
                $data = [];
                if(Auth::user())
                    $data['user_id'] = Auth::user()->id;
                $order = Order::create($data);
                session(['order_id' => $order->id]);
            }else{
                $order =  Order::findOrfail(session('order_id'));
            }
            $this->productIncrement($order, $product->id);
            session()->flash('success', 'Товар '. $product->title .' был добавлен в корзину');
        }else{
            session()->flash('danger', "Продукт '$product->title' недоступен для продажи");
        }

    }

    private function checkAvailable($product){
        return ($product->available) ? true : false;
    }

    public function delete($product){
        if(!is_null(session('order_id'))){
            $order = $this->orderFind($product->id);
            $this->productDecrement($order);
        }
        event(new DeleteOrderEvent(Order::find(session('order_id'))->getFullSum()));
        session()->flash('danger', 'Товар: '.$product->title . ' был удален');
    }


}
