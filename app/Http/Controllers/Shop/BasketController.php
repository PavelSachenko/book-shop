<?php

namespace App\Http\Controllers\Shop;

use App\Events\SendOrderOnEmailEvent;
use App\Http\Controllers\Controller;
use App\Http\Requests\Shop\ConfirmRequest;
use App\Http\SpecialClasses\Basket;
use App\Models\Order;
use App\Models\Rest\Book;
use Illuminate\Http\Request;

class BasketController extends Controller
{
    public function index(){
        $order = Order::find(session('order_id'));
        return view('shop.basket.index', compact('order'));
    }

    public function add(Request $request, Book $product){
        (new Basket)->add($product);
        return redirect()->route('basket.index');
    }

    public function delete(Request $request, Book $product){
        (new Basket())->delete($product);
        return redirect()->route('basket.index');

    }

    public function confirm(){
        $order = Order::find(session('order_id'));
        return view('shop.basket.confirm', compact('order'));
    }

    public function processing(ConfirmRequest $request){
        $params = $request->all();
        $params['status'] = 1;
        $order = Order::find(session('order_id'));
        $order->update($params);
        event(new SendOrderOnEmailEvent($order));
        $this->clear();
        return redirect()->route('shop.index');
    }

    public function clear(){
        session()->forget('order_id');
    }

}
