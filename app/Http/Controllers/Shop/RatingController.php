<?php

namespace App\Http\Controllers\Shop;

use App\Http\Controllers\Controller;
use App\Models\Rating;
use App\Models\Rest\Book;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class RatingController extends Controller
{
    public function add(Request $request, Book $product){
        $raiting = Rating::rating($product->id)->first();
        if($raiting != null){
            $raiting->update(['user_id' => Auth::user()->id, 'book_id' => $product->id, 'rating' => $request->rating]);
        }else{
            Rating::create(['user_id' => Auth::user()->id, 'book_id' => $product->id, 'rating' => $request->rating]);
        }
        return redirect()->route('shop.product.show', $product);
    }
}
