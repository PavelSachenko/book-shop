<?php

namespace App\Http\Controllers\Shop;

use App\Http\Controllers\Controller;
use App\Models\Rest\Book;
use App\Models\Rest\Category;
use Illuminate\Http\Request;

class MainController extends Controller
{
    public function index(){
        $products = Book::where('available', 1)->paginate(6);
        return view('shop.product.index', compact('products'));
    }

    public function show(Book $product){
        return view('shop.product.show', compact('product'));
    }

    public function categories(){
        $categories = Category::all();
        return view('shop.category.index', compact('categories'));
    }

    public function category(Category $category){
        $products = $category->products()->paginate(6);
        return view('shop.product.index', compact('products'));
    }

    public function filter(Request $request){

        $request->validate([
            'min_price' => 'numeric|nullable',
            'max_price' => 'numeric|nullable'
        ]);
        $products = Book::filter($request)->paginate(6);
        return view('shop.product.index', compact('products'));

    }


}
