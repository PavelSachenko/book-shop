<?php

namespace App\Http\Controllers\Shop;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Auth;

class OrderController extends Controller
{
    public function index(){
        $orders = User::find(Auth::user()->id)->orders()->get();
        return view('shop.user.order.index', compact('orders'));
    }
}
