<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Order;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    public function index(){
        $orders = Order::paginate(10);
        return view('admin.order.index', compact('orders'));
    }

    public function show(Order $order){
        return view('admin.order.show', compact('order'));
    }

    public function edit(Order $order){
        return view('admin.order.edit', compact('order'));
    }

    public function update(Request $request, Order $order){
        $order->update($request->all());
        return redirect()->route('admin.orders');
    }
}
