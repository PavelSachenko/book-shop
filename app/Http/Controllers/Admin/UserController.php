<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function index(){
        $users = User::paginate(10);
        return view('admin.users.index', compact('users'));
    }

    public function update(Request $request, User $user){
        $user->update(['role_id' => 3]);
        session()->flash('warning', 'Права обновлены');
        return redirect()->route('admin.users');
    }

    public function destroy(User $user){
        $user->delete();
        session()->flash('danger', 'Пользователь удален');
        return redirect()->route('admin.users');
    }
}
