<?php

namespace App\Http\Controllers\Admin\Product;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\LabelRequest;
use App\Models\Rest\Label;

class LabelController extends Controller
{
    public function index(){
        $labels = Label::paginate(10);
        return view('admin.product.label.index', compact('labels'));
    }

    public function create(){
        return view('admin.product.label.create');
    }

    public function store(LabelRequest $request){
        Label::create($request->all());
        session()->flash('success', 'Лейбл был добавлен');
        return redirect()->route('admin.label.create');
    }

    public function edit(Label $label){
        return view('admin.product.label.create', compact('label'));
    }

    public function update(LabelRequest $request, Label $label){
        $label->update($request->all());
        session()->flash('warning', 'Лейбл был изменен');
        return redirect()->route('admin.label.index');
    }

    public function destroy(Label $label){
        $label->delete();
        return redirect()->back();
    }
}
