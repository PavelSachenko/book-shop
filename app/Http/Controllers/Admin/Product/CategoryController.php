<?php

namespace App\Http\Controllers\Admin\Product;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\CategoryRequest;
use App\Models\Rest\Category;
use Illuminate\Support\Facades\Storage;

class CategoryController extends Controller
{
    public function index(){
        $categories = Category::paginate(10);
        return view('admin.product.category.index', compact('categories'));
    }

    public function create(){
        return view('admin.product.category.create');

    }

    public function store(CategoryRequest $request){
        $params = $request->all();
        if(isset($params['image'])){
            $params['image'] = $request->file('image')->store('public/category');
        }
        Category::create($params);
        session()->flash('success', 'Категория была добавлена');
        return redirect()->route('admin.category.create');
    }

    public function edit(Category $category){
        return view('admin.product.category.create', compact('category'));
    }

    public function update(CategoryRequest $request, Category $category){
        $params = $request->all();
        if(isset($params['image'])){
            $params['image'] = $request->file('image')->store('public/category');
        }
        $category->update($params);
        session()->flash('warning', 'Категория была обновлена');
        return redirect()->route('admin.category.index');
    }

    public function destroy(Category $category){
        Storage::delete($category->image);
        $category->delete();
        return redirect()->back();
    }
}
