<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\SpecialClasses\DataStatistic;

class StatisticController extends Controller
{
    public function index(){
        $data = (new DataStatistic)->getData();
        return view('admin.statistics.index', compact('data'));
    }
}
