<?php

namespace App\Models;

use App\Models\Rest\Book;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $guarded = [];

    public function products(){
        return $this->belongsToMany(Book::class)->withPivot('count');
    }

    public function getFullSum(){
        $price = 0;
        foreach ($this->products()->get() as $product){
            $price += $product->price * $product->pivot->count;
        }
        return $price;
    }

    public function getStatusAttribute($value){
        if($value == 1)
            return $value = "В обработке";
        elseif ($value == 2)
            return $value = "Выполнено";
        elseif ($value == 3)
            return $value = "Отмененно";
        elseif ($value == 0)
            return $value = "В корзине";
        else
            return $value = "Неизвестный статус";
    }

}
