<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Rating extends Model
{
    protected $guarded = [];

    public function scopeRating($query, $product_id){
        return $query->where([['user_id', Auth::user()->id], ['book_id', $product_id]]);
    }

    public static function getRating($book_id){
        $sum = 0;
        $rating = Rating::where('book_id', $book_id)->get();
        if($rating->all() != null){
            foreach ($rating as $item) {
                $sum += $item->rating;
            }
            return $sum / count($rating);
        }
        return 0;
    }
}
