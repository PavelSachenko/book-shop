<?php

namespace App\Models\Rest;

use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    protected $guarded = [];
    protected $table = 'books';

    public function categories(){
        return $this->belongsTo(Category::class, 'category_id');
    }
    public function publishers(){
        return $this->belongsTo(Publisher::class, 'publisher_id');
    }
    public function countries(){
        return $this->belongsTo(Country::class, 'country_id');
    }

    public function labels(){
        return $this->belongsTo(Label::class, 'label_id');
    }

    public function setAvailableAttribute($value){
        $this->attributes['available'] = ($value == 'on') ? 1 : 0;
    }

    public static function filter($request){
        $books = Book::query();
        if($request->filled('title')){
            $books = $books->where("title", 'like', "%$request->title%");
        }
        if($request->filled('min_price') && $request->filled('max_price')){
            $books = $books->whereBetween("price", [$request->min_price, $request->max_price]);
        }
        else if($request->filled('min_price')){
            $books = $books->where("price", '<', $request->min_price);
        }
        else if($request->filled('max_price')){
            $books = $books->where("price", '>', $request->min_price);
        }
        if(isset($request->available)){
            $books = $books->where("available", '=', 1);
        }else{
            $books = $books->where("available", '=', 0);

        }
        return $books;
    }

}
