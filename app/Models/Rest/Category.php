<?php

namespace App\Models\Rest;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $guarded = [];
    protected $table = 'categories';

    public function products(){
        return $this->hasMany(Book::class);
    }
    public function  getRouteKeyName()
    {
        return 'name';
    }
}
