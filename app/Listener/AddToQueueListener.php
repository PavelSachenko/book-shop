<?php

namespace App\Listener;

use App\Events\SendOrderOnEmailEvent;
use App\Jobs\SendEmailJob;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class AddToQueueListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  SendOrderOnEmailEvent  $event
     * @return void
     */
    public function handle(SendOrderOnEmailEvent $event)
    {
        SendEmailJob::dispatch($event->order);
    }
}
