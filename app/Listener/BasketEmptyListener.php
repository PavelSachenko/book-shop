<?php

namespace App\Listener;

use App\Events\DeleteOrderEvent;
use App\Models\Order;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class BasketEmptyListener
{
    public function handle(DeleteOrderEvent $event)
    {
        if($event->sum <= 0){
            Order::find(session('order_id'))->delete();
            session()->forget('order_id');
        }
    }
}
